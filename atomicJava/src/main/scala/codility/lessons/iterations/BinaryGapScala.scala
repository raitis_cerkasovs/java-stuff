package codility.lessons.iterations

/**
  * Created by raitis on 13/02/2016.
  */



object BinaryGapScala extends App {
  def solution(N: Int): Int = {

    val string: String = N.toBinaryString

    var acum: Int = 0
    var result: Int = 0


    for (s <- string) {

      if (s == '0')
        acum += 1
      else {
        if (result < acum)
          result = acum
        acum = 0
      }

    }

    result
  }


  println(
    solution(1041)
  )
  println(
    solution(51712)
  )



}
