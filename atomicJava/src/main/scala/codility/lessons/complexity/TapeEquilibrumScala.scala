package codility.lessons.complexity

/**
  * Created by raitis on 13/02/2016.
  */
object TapeEquilibrumScala extends App {


  // slow, but good solution
  def solution(A: Array[Int]): Int = {

    (for ((a,i) <- A.zipWithIndex) yield Math.abs(A.drop(i).sum - A.take(i).sum)) min

  }


  println(solution(Array(3, 1, 2, 4, 3)))

}
