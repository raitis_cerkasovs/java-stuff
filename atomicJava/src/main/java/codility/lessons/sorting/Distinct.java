package codility.lessons.sorting;

import java.util.Arrays;


/**
 * Created by raitis on 17/02/2016.
 */
public class Distinct {

    public static int solution(int[] A) {

        int count = 1;

        Arrays.sort(A);

        for (int i=1; i<A.length; i++) {
            if (A[i - 1] != A[i]) count++;
        }

        if (A.length == 0)
            return 0;
        else
            return count;
    }


    public static void main(String[] args) {

        int[] arr = {2,1,1,2,3,1};

        System.out.println(solution(arr));
    }
}
