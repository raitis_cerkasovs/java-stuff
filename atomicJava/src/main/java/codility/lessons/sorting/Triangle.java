package codility.lessons.sorting;

import java.util.Arrays;

/**
 * Created by raitis on 17/02/2016.
 */
public class Triangle {

    public static int solution(int[] A) {

        for (int p=0; p<A.length; p++) {

            for (int q=p+1; q<A.length; q++) {

                for (int r=q+1; r<A.length; r++) {

                    if (A[p] + A[q] > A[r] && A[q] + A[r] > A[p] && A[r] + A[p] > A[q]) {

                        return 1;

                    }
                }
            }
        }

        return 0;
    }






    public static void main(String[] args) {

        int[] arr = {10,2,5,1,8,20};
        int[] arr2 = {10,50,5,1};

        System.out.println(solution(arr));
        System.out.println(solution(arr2));

    }
}
