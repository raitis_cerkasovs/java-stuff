package codility.lessons.sorting;


/**
 * Created by raitis on 17/02/2016.
 */
public class MaxProductOfTree {

    public static int solution(int[] A) {

        int result = Integer.MIN_VALUE;

        for (int p=0; p<A.length; p++) {

            for (int q=p+1; q<A.length; q++) {

                for (int r=q+1; r<A.length; r++) {

                    if (A[p] * A[q] * A[r] > result) {

                        result = A[p] * A[q] * A[r];

                    }
                }
            }
        }

        return result;
    }






    public static void main(String[] args) {

        int[] arr = {-3,1,2,-2,5,6};
        // int[] arr2 = {10,50,5,1};

        System.out.println(solution(arr));
       // System.out.println(solution(arr2));

    }
}
