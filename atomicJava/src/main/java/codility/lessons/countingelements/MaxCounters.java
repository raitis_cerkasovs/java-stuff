package codility.lessons.countingelements;

import java.util.Arrays;

/**
 * Created by raitis on 16/02/2016.
 */
public class MaxCounters {

    public static int[] solution(int N, int[] A) {

        int[] result = new int[N];

        for (int i=0; i<A.length; i++) {

            if (A[i] == N + 1) {

                int[] temp = result;
                Arrays.sort(temp);

                for (int j = 0; j < result.length; j++) {
                    result[j] = temp[temp.length-1];
                }

            } else if (A[i] > 0 && A[i] <= result.length)  {
                result[A[i]-1] = result[A[i]-1] + 1;
            }
        }

        return result;
    }





    public static void main(String[] args) {

        int[] arr1 = {3,4,4,6,1,4,4};
        int[] arr2 = {1};

        System.out.println(Arrays.toString(solution(5, arr1)));
        System.out.println(Arrays.toString(solution(1, arr2)));

    }
}
