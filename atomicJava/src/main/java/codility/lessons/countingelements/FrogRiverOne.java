package codility.lessons.countingelements;

/**
 * Created by raitis on 14/02/2016.
 */
public class FrogRiverOne {


    public static int solution(int X, int[] A) {

        int result = -1;
        int positions = X;
        int[] positionsArray = new int[positions+1];

        for (int i=0; i<A.length; i++) {
            if (A[i] < positionsArray.length && positionsArray[A[i]] < 1) {
                positionsArray[A[i]] = A[i];
                positions --;
            }
            if (positions == 0) {
                result = i;
                break;
            }
        }

        return result;
    }





    public static void main(String[] args) {

        int[] arr = {1,3,1,4,2,3,5,4};
        int[] arr1 = {};
        int[] arr2 = {1,3,1,4,2,3,4};
        int[] arr3 = {6};
        int[] arr4 = {3};


        System.out.println(solution(5, arr) + " -> 6");
        System.out.println(solution(5, arr1) + " -> -1");
        System.out.println(solution(5, arr2) + " -> -1");
        System.out.println(solution(5, arr3) + " -> -1");
        System.out.println(solution(5, arr4) + " -> -1");
        System.out.println(solution(1, arr) + " -> 0");



    }

//  sample from Google

//  public static int toDelete(int X, int[] A) {
//            int steps = X;
//            boolean[] bitmap = new boolean[steps + 1];
//
//      System.out.println(Arrays.toString(bitmap));
//
//            for (int i = 0; i < A.length; i++) {
//                if (!bitmap[A[i]]) {
//                    bitmap[A[i]] = true;
//                    steps--;
//                }
//                if (steps == 0) return i;
//            }
//
//      System.out.println(Arrays.toString(bitmap));
//
//            return -1;
//        }



}
