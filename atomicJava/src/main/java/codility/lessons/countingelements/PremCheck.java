package codility.lessons.countingelements;

/**
 * Created by raitis on 16/02/2016.
 */
public class PremCheck {


    public static int solution(int[] A) {

        boolean[] temp = new boolean[A.length+1];
        int counter = A.length;


        for (int i=0; i<A.length; i++) {

            if (A[i] < temp.length && !temp[A[i]]) {
                temp[A[i]] = true;
                counter--;
            }
            if (counter == 0) return 1;

        }

        return 0;
     }







    public static void main(String[] args) {

        int[] arr = {4,1,3,2};
        int[] arr2 = {4,1,3};

        System.out.println(solution(arr));
        System.out.println(solution(arr2));

    }
}
