package codility.lessons.countingelements;

import java.util.Arrays;

/**
 * Created by raitis on 16/02/2016.
 */
public class MissingInteger {



    public static int solution2(int[] A) {

        Arrays.sort(A);

        int result = 1;

        for (int i=0; i<A.length; i++) {
            if (i+1 < A.length && A[i+1] > A[i]+1) {
                return A[i] + 1;
            }
        }

        return result;
    }






    public static int solution(int[] A) {

        Arrays.sort(A);

        int result = 1;

        if (A[0] == 1 && A.length ==1) return 2;
        if (A[0] > 1) return 1;

        for (int i=0; i<A.length; i++) {
            if (A[i] > 0 && i+1 < A.length && A[i+1] > A[i]+1) {
                return A[i] + 1;
            }
        }

        return result;
    }





    public static void main(String[] args) {
        int[] arr = {1,3,6,4,1,2};
        int[] arr3= {3,6,4,2,5};
        int[] arr5= {-1};
        int[] arr6= {-1};
        int[] arr7= {-1,-2};
        int[] arr8= {1};
        int[] arr9= {0};



        System.out.println(solution(arr) + "#5");
        System.out.println(solution(arr3) + "#1");
        System.out.println(solution(arr5) + "#1");
        System.out.println(solution(arr6) + "#1");
        System.out.println(solution(arr7) + "#1");
        System.out.println(solution(arr8) + "#2");
        System.out.println(solution(arr9) + "#1");
    }


}
