package codility.lessons.array;

import java.util.Arrays;

/**
 * Created by raitis on 13/02/2016.
 */
public class OddOccurrencesInArray {


    /**
     * A non-empty zero-indexed array A consisting of N integers is given.
     * The array contains an odd number of elements, and each element of the array
     * can be paired with another element that has the same value, except for one element that is left unpaired.
     *
     * Return the value of the unpaired element.
     *
     * @param A int Array
     * @return int
     */
    public static int solution(int[] A) {

        // sort array
        Arrays.sort(A);

        // assaign first (if the only element)
        int r = A[0];

        // with te step 2 return, if next is not equals
        for (int i=0; i<A.length; i=i+2){

            if (i+2 < A.length && A[i+1] != A[i]) {
                r = A[i];
                break;
            }
        }

        // if the answer is the last element
        if (A.length > 1 && A[A.length-1] != A[A.length-2])
            r = A.length-1;

        return r;
    }


    public static void main(String[] args) {
        int[] arr = {9,3,9,3,9,7,9};
        int[] arr2 = {2, 2, 3, 3, 4};
        int[] arr3 = {2};

        System.out.println(solution(arr));
        System.out.println(solution(arr2));
        System.out.println(solution(arr3));
    }
}



