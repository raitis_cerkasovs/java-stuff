package codility.lessons.iterations.array;

import java.util.Arrays;

/**
 * Created by raitis on 13/02/2016.
 */
public class ArrayRotation {


    /**
     * Move array for one position
     *
     * @param A Array int
     * @return Array int
     */
    public static int[] helper(int[] A) {

            int[] result = new int[A.length];

            result[0] = A[A.length - 1];

            for (int i = 0; i < A.length - 1; i++) {
                result[i + 1] = A[i];
            }
        return result;
    }


    /**
     * Rotation of the array means that each element is shifted right by one index,
     * and the last element of the array is also moved to the first place.
     *
     * For example, the rotation of array A = [3, 8, 9, 7, 6] is [6, 3, 8, 9, 7].
     * The goal is to rotate array A K times; that is, each element of A will be shifted to the right by K indexes.
     *
     * Loop one position movement
     *
     * @param A Array int
     * @param K int
     * @return  Array int
     */
    public static int[] solution(int[] A, int K) {

        if (A.length == 0) return new int[0];

        for (int i=0; i<K; i++){
            A = helper(A);
        }

        return A;
    }







    /**
     * Rotate arry by position K (K only positive)
     *
     * @param A Array int
     * @param K int
     * @return  Array int
     */
    public static int[] movePosition(int[] A, int K) {


        int[] result = new int[A.length];

        if (K>A.length)
            K = K%A.length;

        for (int i=0; i<A.length; i++) {

            if (i+K < A.length)
                result[i] = A[i+K];
            else
                result[i] = A[(i+K)-A.length];

        }

        return result;


    }






    public static void main(String[] args) {

        int[] arr = {1,2,3,4};
        int[] arr2 = {3, 8, 9, 7, 6};
        int[] arr3 = {};

        int[] result1 = movePosition(arr, 1);
        int[] result2 = solution(arr, 3);
        int[] result3 = solution(arr2, 3);
        int[] result4 = solution(arr3, 1);

        //System.out.println(Arrays.toString(result1));
        //System.out.println(Arrays.toString(result2));
        //System.out.println(Arrays.toString(result3));
        System.out.println(Arrays.toString(result4));



    }



}



