package codility.lessons.prefixsums;

/**
 * Created by raitis on 16/02/2016.
 */
public class CountDiv {



    public static int solution(int A, int B, int K) {

        if (A % K == 0) {
            return (B - A) / K + 1;
        } else {
            return (B / K) - (A / K + 1) +1;
        }

    }






    // slow
    public static int solution2(int A, int B, int K) {

        int ac = 0;

        for (int i=A; i<=B; i++) {
            if (i % K == 0) ac++;
        }

        return ac;
    }


    public static void main(String[] args) {
        System.out.println(solution(6,11,2));
        System.out.println(solution(-6,11,2));
        System.out.println(solution(6,-11,2));
        System.out.println(solution(0,0,2));
       // System.out.println(solution(0,0,0));
    }
}
