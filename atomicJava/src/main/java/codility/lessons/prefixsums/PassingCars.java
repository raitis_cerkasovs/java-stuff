package codility.lessons.prefixsums;

/**
 * Created by raitis on 16/02/2016.
 */
public class PassingCars {


    public static int solution(int[] A) {

        int countOnes = 0;
        int countTotal = 0;

        for (int i=A.length-1; i>=0; i--) {

            if (A[i] == 1) {
                countOnes++;
            }
            else if (A[i] == 0) {
                countTotal = countTotal + countOnes;
            }
        }

        if (countTotal > 1000000000 || countTotal < 0)
            return -1;
        else
            return countTotal;
    }



    // slow
    public static int solution2(int[] A) {
        int count = 0;

        for (int i = 0; i < A.length; i++) {
            if (A[i] == 0) {
                for (int j = i; j < A.length; j++) {
                    if (A[j] == 1) {
                        count++;
                    }
                }
            }
        }

        return count;
    }


    public static void main(String[] args) {
        int[] arr = {0, 1, 0, 1, 1};

        System.out.println(solution(arr));
    }
}
