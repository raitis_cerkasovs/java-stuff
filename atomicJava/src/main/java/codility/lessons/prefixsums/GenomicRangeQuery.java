package codility.lessons.prefixsums;

import java.util.Arrays;

/**
 * Created by raitis on 17/02/2016.
 */
public class GenomicRangeQuery {

    // slow
    public static int[] solution(String S, int[] P, int[] Q) {

        // result
        int[] result = new int[P.length];

        // get impact as an int array
        char[] temp = S.toCharArray();
        int[] impact = new int[temp.length];

        for (int i=0; i<temp.length; i++) {
            if (temp[i] == 'A') impact[i] = 1;
            if (temp[i] == 'C') impact[i] = 2;
            if (temp[i] == 'G') impact[i] = 3;
            if (temp[i] == 'T') impact[i] = 4;
        }

        // count
        for (int i=0; i<P.length; i++) {

            int[] delta = new int[Q[i]-P[i]+1];
            int counter = 0;

            for (int j=P[i]; j<=Q[i]; j++) {
                delta[counter] = impact[j];
                counter++;
            }

            Arrays.sort(delta);
            result[i] = delta[0];

        }

        return result;
    }


    /**
     *
     * Slow solution. O(n*m), cannot be loop within the loop.
     *
     * @param S
     * @param P
     * @param Q
     * @return
     */
    public static int[] solution2(String S, int[] P, int[] Q) {

        // result
        int[] result = new int[P.length];

        // get impact as an int array
        char[] temp = S.toCharArray();
        int[] impact = new int[temp.length];

        for (int i=0; i<temp.length; i++) {
            if (temp[i] == 'A') impact[i] = 1;
            if (temp[i] == 'C') impact[i] = 2;
            if (temp[i] == 'G') impact[i] = 3;
            if (temp[i] == 'T') impact[i] = 4;
        }

        // count
        for (int i=0; i<P.length; i++) {

            int[] delta = new int[Q[i]-P[i]+1];
            int counter = 0;

            for (int j=P[i]; j<=Q[i]; j++) {
                delta[counter] = impact[j];
                counter++;
            }

            Arrays.sort(delta);
            result[i] = delta[0];

        }

        return result;
    }






    public static void main(String[] args) {

        String s = "CAGCCTA";
        int[] p = {2,5,0};
        int[] q = {4,5,6};

        System.out.println(Arrays.toString(solution(s, p, q)));
    }

}
