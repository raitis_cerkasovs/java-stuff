package codility.lessons.prefixsums;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by raitis on 17/02/2016.
 */


public class MinAvgTwoSlice {

    /**
     * Still slow, however, uses prefix.
     *
     * @param A
     * @return
     */
    public static int solution(int[] A) {

        int result = Integer.MAX_VALUE;
        double min = Double.MAX_VALUE;

        int[] prefix = new int[A.length+1];
        int acp = 0;


        for (int i=0; i<A.length; i++) {
            acp = acp + A[i];
            prefix[i+1] = acp;
        }


        for (int i=0; i<A.length; i++) {
            for (int j=i+1; j<A.length; j++) {

                double calc = (double)(prefix[j+1] - prefix[i]) / (j - i + 1);

                if (calc < min) {
                    min = calc;
                    result = i;
                } else if (calc == min && result > i) {
                    result = i;
                }

            }
        }

        return result;
    }




    /**
     * Rubbish
     *
     * @param A
     * @return
     */
    public static int solution3(int[] A) {

        ArrayList<Integer> result = new ArrayList();
        ArrayList<Integer> finalResult = new ArrayList();


        for (int i=0; i<A.length; i++) {
            for (int j=i+1; j<A.length; j++) {

                int r = 0;

                for (int d=i; d<=j; d++) {
                    r = r+A[d];
                }

                r = r / (j-i+1);

                result.add(r);

            }
        }


        Collections.sort(result);

        int key = result.get(0);

        for(int i=0; i<A.length; i++) {
            if (A[i] == key)
                finalResult.add(i);
        }

        Collections.sort(finalResult);

        return finalResult.get(0);
    }








    public static void main(String[] args) {

        int[] arr = {4,2,2,5,1,5,8};
        int[] arr2 = {10000, -10000};


        System.out.println(solution(arr));
        System.out.println(solution(arr2));
    }


}
