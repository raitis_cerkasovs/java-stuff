package codility.lessons.maxslice;

/**
 * Created by raitis on 03/03/2016.
 */
public class MaxSliceProblem {

    public static void main(String[] args) {

        int[] arr = {5,-7,3,5,-2,4,-1};
        int[] arr1 = {-7,3,5,-2,4,7,-1};


        // from the book
        int slice=0, end = 0;
        for (int i=0; i<arr.length; i++) {
            end = Math.max(0,end + arr[i] );
            slice = Math.max(slice, end);
        }

        System.out.println("from the book: " + slice);


        // my
        int acum = 0;

        int max = 0;


        for (int i=0; i<arr.length; i++) {

            // 1.
            if (max + arr[i] > 0)
                max = max + arr[i];
            else
                max = 0;


            // 2.
            if (max > acum) acum = max;

        }

        System.out.println("my: " + acum);
    }

}
