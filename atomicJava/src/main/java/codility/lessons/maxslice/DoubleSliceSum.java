package codility.lessons.maxslice;

import java.util.Arrays;

/**
 * Created by raitis on 04/03/2016.
 */
public class DoubleSliceSum {

    public static int solution(int[] A) {

        int temp = A[0];

        Arrays.sort(A);

        int maxSum = 0;


        for (int i=3; i<A.length; i++) {

            maxSum = maxSum + A[i];

        }

     return maxSum-temp;

    }


    public static void main(String[] args) {

        int[] arr = {3,2,6,-1,4,5,-1,2};

        System.out.println(solution(arr));
    }

}
