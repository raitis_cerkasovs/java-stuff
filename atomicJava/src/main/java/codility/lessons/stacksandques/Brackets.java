package codility.lessons.stacksandques;

import java.util.LinkedList;

/**
 * Created by raitis on 18/02/2016.
 */
public class Brackets {


    public static int solution(String S) {

        char[] arr = S.toCharArray();

//        1 '{';
//        2 '[';
//        3 '(';

        int count1 = 0;
        int count2 = 0;
        int count3 = 0;

        LinkedList<Character> openStack = new LinkedList<>();

        for (int i=0; i<arr.length; i++) {

            if (arr[i] == '{') openStack.add(arr[i]);
            if (arr[i] == '[') openStack.add(arr[i]);
            if (arr[i] == '(') openStack.add(arr[i]);


            if (arr[i] == '}' && (openStack.isEmpty() || openStack.getLast() != '{'))
                return 0;
            else if (arr[i] == '}' && openStack.getLast() == '{')
                openStack.removeLast();

            if (arr[i] == ']' && (openStack.isEmpty() || openStack.getLast() != '['))
                return 0;
            else if (arr[i] == ']' && openStack.getLast() == '[')
                openStack.removeLast();

            if (arr[i] == ')' && (openStack.isEmpty() || openStack.getLast() != '('))
                return 0;
            else if (arr[i] == ')' && openStack.getLast() == '(')
                openStack.removeLast();





            if (arr[i] == '{') count1++; else if (arr[i] == '}') count1--;
            if (arr[i] == '[') count2++; else if (arr[i] == ']') count2--;
            if (arr[i] == '(') count3++; else if (arr[i] == ')') count3--;


        }

        if (count1 !=0 || count2!= 0 || count3 != 0) return 0;

       return 1;
    }



    public static void main(String[] args) {

        String s1 = "{[()()]}";
        String s2 = "([)()]";
        String s3 = "}{[()()]}";
        String s4 = "({{({}[]{})}}[]{})";



        System.out.println(solution(s1));
        System.out.println(solution(s2));
        System.out.println(solution(s3));
        System.out.println(solution(s4));





    }
}
