package codility.lessons.stacksandques;

import java.util.LinkedList;

/**
 * Created by raitis on 25/02/2016.
 */

public class StoneWall {


    public static int solution(int[] H) {


        LinkedList<Integer> stack = new LinkedList<>();

        int counter = 0;


        for (int i=0; i<H.length;i++) {

            // each block
            counter++;

            // blocks together
            if (i>0 && H[i-1] == H[i])
                counter--;


            // if next block is higher
            if (i<H.length-1 && H[i] < H[i+1])
                stack.add(H[i]);


            if (i<H.length-1 && H[i] > H[i+1]) {

                int p = stack.size()-1;

                while (p>-1) {

                    if (stack.getLast() > H[i+1]) stack.removeLast();
                    else if (stack.getLast() == H[i+1]) {counter--; break;}
                    else if (stack.getLast() < H[i+1]) break;

                    p--;
                }
            }


        }


        return counter;
    }


    public static void main(String[] args) {

         int[] arr = {8,8,5,7,9,8,7,4,8};

        System.out.println(solution(arr));
    }



}

