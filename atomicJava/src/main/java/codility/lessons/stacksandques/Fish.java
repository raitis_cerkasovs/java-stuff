package codility.lessons.stacksandques;

import java.util.LinkedList;

/**
 * Created by raitis on 22/02/2016.
 */
public class Fish {


    public static int solution(int[] A, int[] B) {


        LinkedList<Integer> stackA = new LinkedList<>();
        LinkedList<Integer> stackB = new LinkedList<>();


        // loop
        for (int i=0; i<B.length; i++) {


            // downstream ->
            if (B[i] == 1) {

                stackB.add(B[i]);
                stackA.add(A[i]);
            }



            // upstream <-
            if (B[i] == 0
                    && stackA.size() > 0
                    && stackB.getLast() == 1
                    && stackA.getLast() > A[i]) {

            }

            else if (B[i] == 0
                    && stackA.size() > 0
                    && stackB.getLast() == 1
                    && stackA.getLast() < A[i]) {




                int counter = stackA.size();

                while (counter > 0) {

                    if (stackA.getLast() < A[i] &&  stackB.get(counter-1) == 1) {

                        stackB.removeLast();
                        stackA.removeLast();

                    }

                    else if (stackA.getLast() > A[i] && stackB.get(counter-1) == 1) {

                        break;
                    }

                    else {

                        stackB.add(B[i]);
                        stackA.add(A[i]);
                        break;

                    }

                    // replace extra loop
                    if (counter == 1) {

                        stackB.add(B[i]);
                        stackA.add(A[i]);
                        break;

                    }

                    counter--;

                }


            }

            else if (B[i] == 0) {

                stackB.add(B[i]);
                stackA.add(A[i]);
            }


        // end loop
        }

        return stackA.size();

    }
















    public static void main(String[] args) {

        int[] arr1a = {4,3,2,1,5};
        int[] arr1b = {0,1,0,0,0};

        int[] arr2a = {0,1};
        int[] arr2b = {1,1};

        int[] arr3a = {4,3,1,5,2,6};
        int[] arr3b = {0,1,0,0,1,0};

        int[] arr4a = {1};
        int[] arr4b = {0};

        int[] arr5a = {1};
        int[] arr5b = {1};

        int[] arr6a = {1,2,3,4,5};
        int[] arr6b = {1,1,1,1,1};

        int[] arr7a = {1,2,3,4,5};
        int[] arr7b = {0,0,0,0,0};

        int[] arr8a = {4,3,2,5,1};
        int[] arr8b = {1,1,1,0,1};

        System.out.println(solution(arr1a, arr1b) + " 2");
        System.out.println(solution(arr2a, arr2b) + " 2");
        System.out.println(solution(arr3a, arr3b) + " 3");
        System.out.println(solution(arr4a, arr4b) + " 1");
        System.out.println(solution(arr5a, arr5b) + " 1");
        System.out.println(solution(arr6a, arr6b) + " 5");
        System.out.println(solution(arr7a, arr7b) + " 5");
        System.out.println(solution(arr8a, arr8b) + " 2");
    }
}

