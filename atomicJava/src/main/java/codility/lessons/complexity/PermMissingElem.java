package codility.lessons.complexity;

import java.util.Arrays;

/**
 * Created by raitis on 13/02/2016.
 */
public class PermMissingElem {


    public static int solution(int[] A) {

        int result = 1;

        Arrays.sort(A);

        for (int i=0; i<A.length-1; i++) {
            if (A[i+1] != A[i]+1)
                result = A[i] + 1;
        }

        if (A.length > 0 && A[0] == 1 && result == 1)
            return A[A.length-1]+1;
        else
            return result;
    }





    public static void main(String[] args) {

        int[] arr = {2,3,1,5};
        int[] arr2 = {};
        int[] arr3 = {2,3,4,5};
        int[] arr4 = {2,3,4,1};

        System.out.println(solution(arr));
        System.out.println(solution(arr2));
        System.out.println(solution(arr3));
        System.out.println(solution(arr4));
    }

}

