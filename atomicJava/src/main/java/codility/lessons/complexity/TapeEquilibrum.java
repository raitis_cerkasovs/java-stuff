package codility.lessons.complexity;

import java.util.Arrays;
import java.util.stream.IntStream;

/**
 * Created by raitis on 13/02/2016.
 */
public class TapeEquilibrum {




    // 100% solution, 0(n)
    public static int solution(int[] A) {

        int ac = 2147483647;

        int[] temp1 = new int[A.length-1];
        int[] temp2 = new int[A.length-1];


        temp1[0] = A[0];
        for (int i=1; i<A.length-1; i++) {
            temp1[i] = temp1[i-1] + A[i];
        }

        temp2[temp2.length-1] = A[A.length-1];

        for (int i=1; i<A.length-1; i++) {
            temp2[temp2.length-1-i] = temp2[temp2.length-i] + A[A.length-1-i];
        }

        for (int i=0; i<temp1.length; i++) {
            if (Math.abs(temp1[i] - temp2[i]) < ac)
                ac = Math.abs(temp1[i] - temp2[i]);
        }

        return ac;
    }









    // also slow, Java8
    public static int solution2(int[] A) {

        int ac = 2147483647;

        for (int i=1; i<A.length; i++) {

            int[] firstArray = Arrays.copyOf(A, i);
            int[] secondArray = Arrays.copyOfRange(A, i, A.length);

            int suma = IntStream.of(firstArray).sum();
            int sumb = IntStream.of(secondArray).sum();

            if (Math.abs(suma-sumb) < ac)
                ac = Math.abs(suma-sumb);

        }

        return ac;
    }





    // good, but fail on time performance 0(n*n)
    public static int solution1(int[] A) {

        int ac = 2147483647;

        for (int i=1; i<A.length; i++) {

            int suma =0;
            int sumb =0;

            for (int a=0; a<i; a++ ){
                suma = suma+A[a];
            }

            for (int b=i; b<A.length; b++ ){
                sumb = sumb+A[b];
            }

            if (Math.abs(suma-sumb) < ac)
                ac = Math.abs(suma-sumb);

        }

        return ac;
    }




    public static void main(String[] args) {

        int[] arr = {3,1,2,4,3};

        System.out.println(solution(arr));

    }
}
