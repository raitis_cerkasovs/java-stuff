package codility.lessons.complexity;

/**
 * Created by raitis on 13/02/2016.
 */
public class SmallFrog {

    public static int solution(int X, int Y, int D) {

        if (((Y - X) % D) > 0)
           return ((Y - X) / D) + 1;
        else
            return ((Y - X) / D);
    }

    public static void main(String[] args) {
        System.out.println(solution(10,85,30));
    }
}
