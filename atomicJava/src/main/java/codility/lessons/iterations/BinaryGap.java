package codility.lessons.iterations;

/**
 * Created by raitis on 06/02/2016.
 */

class Solution {
    public static int solution(int N) {

        int result = 0;
        int temp = 0;

        char[] arr = Integer.toBinaryString(N).toCharArray();


        for (int i=0; i<arr.length; i++) {

            if (arr[i] == '0') {
                temp++;
            } else {
                if (temp > result) {
                    result = temp;
                }
                temp = 0;
            }

        }
        return result;
    }







}


public class BinaryGap {

    public static void main(String[] args) {

        System.out.println(
                Integer.toBinaryString(51712)
        );

        System.out.println(
                Solution.solution(51712)
        );
        System.out.println(
                Integer.toBinaryString(6)
        );

        System.out.println(
                Solution.solution(6)
        );

    }

}



