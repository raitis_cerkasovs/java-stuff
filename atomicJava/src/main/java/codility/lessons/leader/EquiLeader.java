package codility.lessons.leader;


/**
 * Created by raitis on 26/02/2016.
 */

// 100 points

public class EquiLeader {
    public static int solution(int[] A) {

        int equiLeaders = 0;

        // find leader
        int value = -1;
        int size = 0;

        for (int i=0; i<A.length; i++) {
            if (size == 0) {
                value = A[i];
                size++;
            } else if (size > 0) {
                if (value == A[i]) {
                    size++;
                } else {
                    size--;
                }
            }
        }

        // make sure it is a leader and find total amount of leaders
        int total = 0;

        for (int i=0; i<A.length; i++) {
            if  (A[i] == value) total++;
        }


        // if leader find equiLeaders
        int currentLeaders = 0;

        if (total > A.length/2) {


            for (int i=0; i<A.length; i++) {


                if (currentLeaders > i/2 && (total - currentLeaders) > (A.length - i)/2) {
                    equiLeaders++;
                }

                if (A[i] == value) currentLeaders++;

            }

        }


        // return
        return equiLeaders;

    }









    public static void main(String[] args) {

        int[] arr1 = {4, 3, 4, 4, 4, 2};

        System.out.println(solution(arr1));


    }












//    BY FINDING DOMINATOR AS A MIDDLE ELEMENT
//      CORRECTNESS 100% BUT SLOW


//    public static int getLeader(LinkedList<Integer> S) {
//
//        LinkedList<Integer> s = (LinkedList<Integer>) S.clone();
//
//        s.sort(null);
//
//        int count = 0;
//
//        int candidate = s.get(s.size()/2);
//
//        for (int i=0; i<s.size(); i++) {
//
//            if (candidate == s.get(i)) count++;
//
//            if (count > s.size() / 2) return candidate;
//        }
//
//        return -1;
//    }
//
//
//
//
//
//
//
//
//
//
//    public static int solution(int[] A) {
//
//        int[] sortedA = A.clone();
//
//        Arrays.sort(sortedA);
//
//        int leader = sortedA[sortedA.length/2];
//
//        int counter = 0;
//
//        LinkedList<Integer> full = new LinkedList<>();
//        for (int i=0; i<A.length; i++) {
//            full.add(A[i]);
//        }
//        LinkedList<Integer> empty = new LinkedList<>();
//
//
//
//
//
//
//        for (int i=0; i<A.length-1; i++) {
//
//            full.removeFirst();
//            empty.add(A[i]);
//
//            if (leader == getLeader(full) && leader == getLeader(empty))
//                counter++;
//
//        }
//
//        return counter;




























//    REMOVE PAIRS VAARIANT
//    ALSO SLOW !!!


//    public static int getLeader(LinkedList<Integer> S) {
//
//
//        int value = -1;
//        int size = 0;
//
//        for (int i=0; i<S.size(); i++) {
//            if (size == 0) {
//                value = S.get(i);
//                size++;
//            } else if (size > 0) {
//                if (value == S.get(i)) {
//                    size++;
//                } else {
//                    size--;
//                }
//            }
//        }
//
//
//        // count and return
//        int dominator = 0;
//
//        for (int i=0; i<S.size(); i++) {
//            if (S.get(i) == value) dominator++;
//            if (S.size()/2 < dominator) return value;
//        }
//
//
//        return -1;
//    }
//
//
//
//
//
//
//    public static int solution(int[] A) {
//
//        // find leader and prepear stacks
//        LinkedList<Integer> full = new LinkedList<>();
//        LinkedList<Integer> empty = new LinkedList<>();
//
//        int value = -1;
//        int size = 0;
//
//
//
//        for (int i=0; i<A.length;i++) {
//
//            full.add(A[i]);
//
//            if (size == 0) {
//                size++;
//                value = A[i];
//            } else if (size > 0) {
//                if (value == A[i]) {
//                    size++;
//                } else {
//                    size--;
//                }
//            }
//        }
//
//
//
//
//
//        // count EquilLeaders
//        int countLeaders = 0;
//
//
//        for (int i=0; i<A.length-1; i++) {
//
//            full.removeFirst();
//            empty.add(A[i]);
//
//            if (value == getLeader(full) && value == getLeader(empty))
//                countLeaders++;
//
//        }
//
//
//        // return
//        return countLeaders;
}