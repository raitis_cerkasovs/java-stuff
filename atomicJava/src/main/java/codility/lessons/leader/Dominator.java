package codility.lessons.leader;

import java.util.Arrays;

/**
 * Created by raitis on 25/02/2016.
 */
public class Dominator {

    // BY SORTING AND FINDING THE MIDDLE ELEMENT


    public static int solution(int[] A) {

        // if empty array
        if (A.length == 0) return -1;


        // find dominator
        int dominator = -1;

        int[] sortedArr = A.clone();

        Arrays.sort(sortedArr);

        int candidate = sortedArr[sortedArr.length/2];
        int counter = 0;

        // count if candidate is dominator
        for (int i=0; i<A.length; i++) {
            if (A[i] == candidate) counter++;
            if (counter > A.length/2) {
                dominator = candidate;
                break;
            }
        }


        // find element
        if (dominator > 0) {
            for (int i=0; i < A.length; i++) {
                if (A[i] == dominator) return i;
            }
        }


        return -1;
    }




    public static void main(String[] args) {

        int[] arr1 = {3,4,3,2,3,-1,3,3};
        int[] arr2 = {};

        System.out.println(solution(arr1));
        System.out.println(solution(arr2));
    }


}

