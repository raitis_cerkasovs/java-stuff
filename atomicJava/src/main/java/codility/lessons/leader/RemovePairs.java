package codility.lessons.leader;

/**
 * Created by raitis on 27/02/2016.
 */
public class RemovePairs {


    public static void main(String[] args) {

        // BY REMOVING UNEQUAL PAIRS

        // int[] arr1 = {3,4,3,2,3,-1,3,3};
        // int[] arr1 = {1};
        int[] arr1 = {4, 3, 4, 4, 4, 2};


        int size = 0;
        int count = 0;
        int value = -1;


        // remove pairs
        for (int i = 0; i < arr1.length; i++) {

            if (size == 0) {
                value = arr1[i];
                size++;
            } else if (size > 0) {

                if (value == arr1[i])
                    size++;
                else
                    size--;

            }
        }


        // loop to count dominator
        if (size > 0) {
            for (int j = 0; j < arr1.length; j++) {

                if (arr1[j] == value) count++;

            }

        }


        // output
        if (arr1.length / 2 < count)
            System.out.println(value);
        else
            System.out.println(-1);


    }

}
