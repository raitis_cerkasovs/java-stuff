package codility.ocado;

/**
 * Created by raitis on 29/01/2016.
 */
public class TaskOne {


    public static int solution(int Q) {

           // there is no point calculate, negative value is impossible by two squares
           if (Q < 0)  return 0;
           if (Q == 0) return 1;

           int counter = 0;

           for (int i=0; i<= Q; i++) {
               if (((i*i) + (Q-i)*(Q-i)) == Q || ((i*i) + (i*i)) == Q) {
                   counter += 1;
               }
           }

        // it is always going to be 4 variations against position of negative values
        return counter * 4;
    }


    public static void main(String[] args) {

       // int[] a = new int[] {2,-4,6,-3,9};


        System.out.println(
                   solution(8)
        );

    }

}
