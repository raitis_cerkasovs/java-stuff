package codility.ocado;

/**
 * Created by raitis on 29/01/2016.
 */
public class TaskTwo {



    public static int solution(int[] A) {

        int minDist = 2147483647;

        for (int i = 0; i<A.length; i++) {
            for (int ii = 0; ii<A.length; ii++) {

                if (i != ii)

                {
                    ///

                    if (A[i] - A[ii] >= 0 && minDist > A[i] - A[ii])
                        minDist = A[i] - A[ii];
                    else if (A[i] - A[ii] < 0 && minDist > A[ii] - A[i])
                        minDist = A[ii] - A[i];

                    ///
                }

            }
        }

        return minDist;

    }





    public static void main(String[] args) {

        int[] a = new int[] { 8,24,3,20,1,17 };
        int[] b = new int[] { 7,21,3,42,3,7};

        System.out.println(
                solution(a)
        );

        System.out.println(
                solution(b)
        );

    }

}
