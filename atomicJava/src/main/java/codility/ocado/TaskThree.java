package codility.ocado;

/**
 * Created by raitis on 29/01/2016.
 */
public class TaskThree {


    public static int solution(int A, int B) {

        int counter = 0;

        for (int i = A; i<=B; i++ ) {

            Boolean isPrime = true;

            for (int n=2; n<i; n++) {

                if (i%n == 0) isPrime = false;

            }

            if (isPrime) counter += 1;


        }
       return counter;
    }


    public static void main(String[] args) {

       // int[] a = new int[] {2,-4,6,-3,9};

        System.out.println(
                solution(11, 19)
        );

//        System.out.println(
//                8%5
//        );

    }

}
