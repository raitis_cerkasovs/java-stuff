package codility;

/**
 * Created by raitis on 29/01/2016.
 */
public class MinAbsSliceSum {

    public static int solution(int[] A) {

        int temp = 0;
        int acum = 2147483647;


        for (int i=0; i<A.length; i++) {
            for (int ii=0; ii<A.length; ii++) {

                if (0 <= i && i <= ii && ii < A.length) {

                    ////

                    for (int j = i; j <= ii; j++) {
                        temp = temp + A[j];
                    }

                    if (Math.abs(temp) < acum) {
                        acum = Math.abs(temp);
                    }

                     System.out.println(" i->" + i + " ii->" + ii + " slice->" + Math.abs(temp));

                    temp = 0;

                    ////

                }

            }
        }


       return acum;
    };









    public static void main(String[] args) {

        int[] a = new int[] {2,-4,6,-3,9};

        System.out.println(
                solution(a)
        );

    }
}
