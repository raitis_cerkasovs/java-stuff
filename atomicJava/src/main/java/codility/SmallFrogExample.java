package codility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by raitis on 29/01/2016.
 */

public class SmallFrogExample {

    public static int solution(int[] A, int X, int D) {

        // if frog can jump in one jump
        if (D >= X) return 0;

        List<Integer> temp = new ArrayList<Integer>();
        temp.add(0);
        temp.add(X);

        int counter = -1;
        Boolean stop = false;

        for (int i = 0; i < A.length; i++) {

            // if leave is landing into pond
            if (A[i] > 0 && A[i] < X) {

                // stop unnecessary loop if frog can jump
                if (stop) break;
                stop = true;

                // leave falls
                temp.add(A[i]);
                Collections.sort(temp);

                // check each distance within the pond
                for (int j = 0; j < temp.size(); j++) {

                    // index outbond for j+1
                    if (j + 1 < temp.size()) {

                        // check if there is distance further than frog can jump
                        if ((temp.get(j + 1) - temp.get(j)) > D) {
                            counter++;
                            stop = false;
                        }
                    }
                }
            }
        }


        // adjust counter to start from 0 if frog cannot jump at beginning
        if (counter > -1) counter++;

        return counter;
    }








    public static void main(String[] args) {

        int[] a = new int[]{1, 3, 1, 4, 2, 5};
        System.out.println(
                solution(a, 7, 3)
        );
    }
}


//    public static int solution(int[] A, int X, int D) {
//
//        int time = 0;
//        int distance = 0;
//
//        if (D >= X)
//            return time;
//
//        for (int i=0; i<A.length; i++) {
//
//            if (distance + D > X)
//                break;
//
//            if (A[i] <= D) {
//                time += i;
//                distance += A[i];
//            }
//
//        }
//
//        if (distance + D < X)
//            return -1;
//        else
//            return time;
//    }