package testforeverything;

/**
 * Created by raitis on 28/01/2016.
 */


class Testme {

    static String myStaticFunction(String s) {
        return s;
    }
}

class Testme2 {

    static String myStaticFunction2(String s) {
        return s;
    }
}

public class StaticClasses extends Testme2 {

    // CANNOT OVERRIDE STATIC FUNCTIONS
    // public String myStaticFunction2(String s) {
    //      return s+s;
    // }

    public static void main(String[] args) {
        System.out.println("Hallo MaxCounters");

        System.out.println(
                Testme.myStaticFunction("Some static class")
        );
    }
}
