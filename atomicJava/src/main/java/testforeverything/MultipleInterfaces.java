package testforeverything;

/**
 * Created by raitis on 28/01/2016.
 */

interface Run1 {
    public void run();
    public Integer go();
}

interface Run2 {
    void run();
}

// Abstract classes can implement interfaces without even providing the implementation of interface methods.

// Variables declared in a MaxCounters interface is by default final. An abstract class may contain non-final variables.

public class MultipleInterfaces implements Run1, Run2 {

    @Override
    public Integer go() {
        return null;
    }

    @Override
    public void run() {
    }

    public static void main(String[] args) {
        System.out.println("ok");
    }
}
