package mystringutils;

/**
 * Created by raitis on 08/02/2016.
 */
public class StringUtils {

    public static String reverse(String s) {

        char[] arr = s.toCharArray();
        int length = arr.length;

        for (int i=0; i<length/2; i++) {
            char temp;
            temp = arr[i];
            arr[i] = arr[length-1-i];
            arr[length-1-i] = temp;
        }

        return new String(arr);
    }






    public static void main(String[] args) {

        System.out.println(
                reverse("okpok")
        );

        System.out.println(
               // 13/2
        );

    }


}
