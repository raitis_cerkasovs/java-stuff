package mystringutils;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by raitis on 08/02/2016.
 */
public class TestStringUtils {


    StringUtils stringUtils = new StringUtils();

    @Test
    public void testForEvenLengthStrings() {

        String testStringEven = "myass";

        String correctOutputEven = "ssaym";

        String resultEven = stringUtils.reverse(testStringEven);

        assertEquals("Test for even length string.", correctOutputEven, resultEven);

    }


    @Test
    public void testForOddLengthStrings() {

        String testStringOdd = "latvia";

        String correctOutputOdd = "aivtal";

        String resultOdd = stringUtils.reverse(testStringOdd);

        assertEquals("Test for odd length string.",correctOutputOdd, resultOdd);

    }


    @Test
    public void testForLongStrings() {

        String testStringOdd = "arbitrazhastiesa";

        String correctOutputOdd = "aseitsahzartibra";

        String resultOdd = stringUtils.reverse(testStringOdd);

        assertEquals("Test for odd length string.",correctOutputOdd, resultOdd);

    }




}
