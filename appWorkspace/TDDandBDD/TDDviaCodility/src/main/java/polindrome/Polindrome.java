package polindrome;

/**
 * Created by raitis on 03/02/2016.
 */
public class Polindrome {

    public static  Boolean reverse(String s) {

        if (new StringBuilder(s).reverse().toString().equals(s))
            return true;
        return false;

    }


    public static String longest(String s) {

        String currentLongest = "";

        String reverseS = new StringBuilder(s).reverse().toString();

        for (int i=0; i<=s.length(); i++) {

            if (reverse(s.substring(i)) && s.substring(i).length() > currentLongest.length())
                currentLongest = s.substring(i);

            if (reverse(reverseS.substring(i)) && reverseS.substring(i).length() > currentLongest.length())
                currentLongest = reverseS.substring(i);
        }

       return currentLongest;
    }









    public static void main(String[] args) {


        System.out.println(

                longest("abaxzzzz")

        );
    }

}
