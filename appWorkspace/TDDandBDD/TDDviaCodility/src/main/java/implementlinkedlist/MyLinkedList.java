package implementlinkedlist;


/**
 * Created by raitis on 03/02/2016.
 */


class MyLinkedList {


   // for Java style LinkedList probably need wrapper class and index(?)
   // public Integer index = 0;

    public Integer head = null;
    public MyLinkedList tail = null;


    MyLinkedList() {}


    MyLinkedList(Integer head, MyLinkedList tail) {
        this.head = head;
        this.tail = tail;
    }


    public MyLinkedList add(Integer head) {
        return new MyLinkedList(head, this);
    }




    public void print() {

        MyLinkedList l = this;

        while (l.tail != null) {
            System.out.println(l.head);
            l = l.tail;
        }
    }


}











class Output {

    public static void main(String[] args) {

        MyLinkedList myLinkedList = new MyLinkedList();

        myLinkedList.add(1).add(2).add(3).add(5).print();

    }

}
