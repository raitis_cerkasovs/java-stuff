package file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by raitis on 03/02/2016.
 */

// Count nr of files in the subdirectories

public class FileOperations {


    private int count = 0;
    private static int totalLines = 0;


    public int countFiles(String path) {

        File f = new File(path);
        File[] files = f.listFiles();

        if (files != null)
            for (int i = 0; i < files.length; i++) {

                count++;
                File file = files[i];

                if (file.isDirectory()) {
                    countFiles(file.getAbsolutePath());
                    count--;
                }
            }

        return count;
    }

// Count nr of lines in files of subdirectories

    public static int countLines(String path) throws IOException {

        File f = new File(path);
        File[] files = f.listFiles();

        if (files != null)
            for (int i = 0; i < files.length; i++) {

                File file = files[i];

                if (file.isDirectory()) {
                    countLines(file.getAbsolutePath());
                }

                else if (file.isFile()) {

                    // line count

                    BufferedReader reader = new BufferedReader(new FileReader(file.getAbsolutePath()));

                    int lines = 0;
                    while (reader.readLine() != null) lines++;
                    reader.close();

                    totalLines += lines;

                    // line count end

                }

            }

        return totalLines;
    }




    public static void main(String[] args) throws IOException {
        System.out.println(countLines("/Users/raitis/Dropbox/java/appWorkspace/TDDandBDD/TDDviaCodility/src/input"));
    }

}
