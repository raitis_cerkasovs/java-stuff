package file;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.junit.runners.JUnit4;

import java.io.IOException;

/**
 * Created by raitis on 02/02/2016.
 */
public class TestForFileOperations {


    FileOperations fileOperations = new FileOperations();


    @Test
    public void testFileCount() {

        int expectedOutput = 7;

        int output = fileOperations.countFiles("/Users/raitis/Dropbox/java/appWorkspace/TDDandBDD/TDDviaCodility/src/input");

        assertEquals("File number must match", expectedOutput, output);

    }


    @Test
    public void testLineCount() throws IOException {

        int expectedOutput = 15;

        int output = fileOperations.countLines("/Users/raitis/Dropbox/java/appWorkspace/TDDandBDD/TDDviaCodility/src/input");

        assertEquals("File lines must match", expectedOutput, output);

    }


}
