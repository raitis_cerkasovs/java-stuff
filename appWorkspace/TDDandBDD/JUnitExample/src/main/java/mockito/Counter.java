package mockito;

/**
 * Created by raitis on 01/02/2016.
 */
public class Counter {

    Counter() {
        this.increment = 1;
    }

    private int increment;

    public Integer getValue() {
        return increment++;
    }

}
