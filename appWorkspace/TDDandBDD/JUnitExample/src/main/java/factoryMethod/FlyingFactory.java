package factoryMethod;

/**
 * Created by raitis on 01/02/2016.
 */
public class FlyingFactory {



    public Flying create(String s) {

        if (s.equals("Fighter Jet"))
            return new FlyLikeFJ();

        return null;
    }



}
