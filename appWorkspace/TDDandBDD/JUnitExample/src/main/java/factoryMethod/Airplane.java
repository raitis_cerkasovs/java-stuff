package factoryMethod;

/**
 * Created by raitis on 01/02/2016.
 */
public class Airplane {

    private LiftOff lift;
    private Flying fly;



    public Airplane(LiftOff lift, Flying fly) {
        this.lift = lift;
        this.fly = fly;
    }

    public String howDoIfly() {
        return "myOutput";
    }
}
