package designPaterns;

import factoryMethod.*;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by raitis on 01/02/2016.
 */
public class TestFactoryMethod {


    private static FlyingFactory flyingFactory;

    @BeforeClass
    public static void onlyOnce() {
        flyingFactory = new FlyingFactory();
    }

    @Test
    public void test1() {

        String expectedOutput = "myOutput";
        String stringreturned = null;

        Flying fly = flyingFactory.create("Fighter Jet");
        LiftOff liftoff = new LiftOffV();

        Airplane classUnderTest = new Airplane(liftoff, fly);

        stringreturned = classUnderTest.howDoIfly();

        assertEquals("myOutput", expectedOutput, stringreturned);



    }



}
