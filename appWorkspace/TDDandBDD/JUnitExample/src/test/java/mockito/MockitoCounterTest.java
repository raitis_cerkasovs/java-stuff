package mockito;

/**
 * Created by raitis on 01/02/2016.
 */

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;



public class MockitoCounterTest {



    @Test
    public void test() {

        Integer first;
        Integer expectedFirst = 1;

        Counter count = new Counter();
        first = count.getValue();

        assertEquals("Right Answer !", expectedFirst, first);

    }




    @Test
    public void testMockito() {

        Integer first, mockedFirst;
        Integer expectedFirst = 22;
        Integer expectedTwentyThree = 23;

        Counter count = new Counter();

        // mocking
        Counter mockedCounter = mock(Counter.class);
        when(mockedCounter.getValue()).thenReturn(22).thenReturn(23);

        // class method must be defined
        // when(mockedCounter.setValue())

        // thenReturn 22
        mockedFirst = mockedCounter.getValue();
        assertEquals("Twenty two", expectedFirst, mockedFirst);

        // thenReturn 23
        mockedFirst = mockedCounter.getValue();
        assertEquals("Twenty two", expectedTwentyThree, mockedFirst);


        // atLeast(), atMost(), atLeastOnce(), never()
        // inOrder()
        // doThrow(newIllegalArgumentException()).when(mockedDivider).divide(anyInt(),eq(0));
        // Can you think of a simple way to do this without Mockito

    }

}



